import mariadb from 'mariadb';

/**
 * Class dbConnect
 * @return {mariadb.Pool}
 */
export class DBConnectClass {
  private readonly connexion: mariadb.Pool;

  /**
   * Constructor of class dbConnect
   */
  constructor() {
    this.connexion = this.initConnexion();
  }

  private initConnexion = (): mariadb.Pool => {
    return mariadb.createPool({
      host: 'localhost',
      user: 'admin',
      password: 'admin',
      database: 'url_shortener',
      port: 3306,
    });
  }

  /**
   * @return {mariadb.Pool}
   * @private
   */
  getConnexion() {
    return this.connexion;
  }
}

const dbConnect = new DBConnectClass();
export default dbConnect;
