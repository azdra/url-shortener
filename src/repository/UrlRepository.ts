import {DBConnectClass} from '../manager/dbConnect';

/**
 * class UrlRepository
 */
export class UrlRepositoryClass extends DBConnectClass {
  /**
   * findOneByShort
   * @param {string} shortUrl
   * @protected
   * @return {Promise<void>}
   */
  public findOneByShort(shortUrl: string): Promise<any> {
    return this.getConnexion().query('SELECT * FROM url WHERE short = ?', [
      shortUrl,
    ]);
  }

  /**
   * insert
   * @param {string} url
   * @param {string} shortUrl
   * @return {Promise<void>}
   */
  public insert(url:string, shortUrl: string): Promise<any> {
    return this.getConnexion().query('INSERT INTO url VALUE (null, ?, ?, 0)', [
      url, shortUrl,
    ]);
  }
}

const urlRepository = new UrlRepositoryClass();
export default urlRepository;
