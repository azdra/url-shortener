import express, {Express} from 'express';
import bodyParser from 'body-parser';
import * as ejs from 'ejs';
import cors from 'cors';
import urlRepository from './repository/UrlRepository';

const app: Express = express();
const PORT = 3000;

app.engine('ejs', ejs.__express);
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use(express.json());
app.use(express.static('./public'));

app.get('/', (req, res) => {
  res.render('home/index');
});

app.post('/shortUrls', (async (req, res) => {
  const {fullUrl, shortCode} = req.body;

  if (!fullUrl) {
    return res.render('home/index', {
      test: 'mfqsfjqsoihfsqoihfio',
    });
  }

  const valid = await urlRepository.insert(fullUrl, shortCode);

  if (valid) {
    res.redirect('/');
  }
}));

app.get('/:short', (async (req, res) => {
  const {short} = req.params;
  const shortUrl = await urlRepository.findOneByShort(short);

  if (shortUrl && shortUrl[0]) {
    const {full} = shortUrl[0];
    return res.redirect(full);
  }

  return res.send('None shall pass');
}));

app.listen(PORT, () => console.log('Web app available at http://localhost:%s', 8000));
